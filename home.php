<?php
/**
 * Template Name: Home Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package dsframework
 * @since dsframework 1.0
 */

get_header(); ?>
		<div id="primary" class="site-content">
			<div id="content" role="main">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'page' ); ?>
				<?php endwhile; ?>
			</div>
		</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>

<script type="text/javascript">
	//jQuery(document).ready(function($) {
		/* forward to first project */
		if (window.location.href == "http://cosmic.es/" || 
			window.location.href == "http://www.cosmic.es/" ||
			window.location.href == "http://cosmic.es/es/" || 
			window.location.href == "http://www.cosmic.es/es/" ||
			window.location.href == "https://cosmic.es/" || 
			window.location.href == "https://www.cosmic.es/" ||
			window.location.href == "https://cosmic.es/es/" || 
			window.location.href == "https://www.cosmic.es/es/") {

			var gallery = "galeria/proyectos/";
			//var startHash = "#albert-y-ferran-adria";
			var startHash = "#<?php echo get_ds_option('fb_admin_id') ?>";
			location = window.location.href + gallery + startHash;
		}
	//}
</script>