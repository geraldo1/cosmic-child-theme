<?php

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    //wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css'
    );
}

//preload CSS and Javascript files for Royalslider on every page
//register_new_royalslider_files(1);

function load_cosmic_scripts() {
	wp_enqueue_script(
		'cosmic-script',
		get_stylesheet_directory_uri() . '/cosmic.js',
		array( 'jquery' )
	);
}
add_action( 'wp_enqueue_scripts', 'load_cosmic_scripts' );

add_action( 'add_meta_boxes', 'proyecto_meta' );
function proyecto_meta() {
    add_meta_box( 'proyecto_meta', 'Proyecto', 'proyecto_meta_feedback', 'ds-gallery', 'normal', 'high' );
}

function proyecto_meta_feedback( $post ) {
    $proyecto = get_post_meta( $post->ID, '_proyecto', true);
    echo 'Nombre del proyecto';
    ?>
    <input type="text" id="proyecto" name="proyecto" value="<?php echo esc_attr( $proyecto ); ?>" />
    <?php
}

add_action( 'save_post', 'save_proyecto_meta' );
function save_proyecto_meta( $post_ID ) {
    global $post;
    if( $post && $post->post_type == "ds-gallery" ) {
	    if (isset( $_POST ) ) {
	        update_post_meta( $post_ID, '_proyecto', strip_tags( $_POST['proyecto'] ) );
	    }
	}
}

// Move TinyMCE Editor to the bottom
add_action( 'add_meta_boxes', 'action_add_meta_boxes', 0 );
function action_add_meta_boxes() {
    global $_wp_post_type_features;
        if (isset($_wp_post_type_features['ds-gallery']['editor']) && $_wp_post_type_features['ds-gallery']['editor']) {
            unset($_wp_post_type_features['ds-gallery']['editor']);
            add_meta_box(
                'description_section',
                __('Description'),
                'inner_custom_box',
                    'ds-gallery', 'normal', 'low'
            );
        }
add_action( 'admin_head', 'action_admin_head'); //white background
}
function action_admin_head() {
    ?>
    <style type="text/css">
        .wp-editor-container{background-color:#fff;}
        #proyecto{width:100%;}
    </style>
    <?php
}
function inner_custom_box( $post ) {
    echo '<div class="wp-editor-wrap">';
    wp_editor($post->post_content, 'content', array('dfw' => true, 'tabindex' => 1) );
    echo '</div>';
}

//filter( 'manage_ds-gallery_posts_columns', 'set_custom_edit_columns' );
add_action( 'manage_ds-gallery_posts_custom_column' , 'custom_columns', 10, 2 );

function set_custom_edit_columns($columns) {
    unset( $columns['ds-gallery-category'] );
    $columns['proyecto'] = __( 'Proyecto', 'touchfolio-child' );
    return $columns;
}

function custom_columns( $column, $post_id ) {
    switch ( $column ) {
        case 'ds-gallery-category':
            $proyecto = get_post_meta( $post_id, '_proyecto', true);
            echo " | ".wpm_translate_string($proyecto); 
            break;
    }
}
?>