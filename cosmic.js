jQuery(document).ready(function($) {

	/*$("#page").on("click", function() { 
		var video = $(".vidbg-container video").get(0);
		if (video) {
			video.muted = true;
			if (video.paused) video.play(); 
			//else video.pause(); 
		}
	});*/

	/* maintain hash on language change */
	if(window.location.hash) {
		var hash = window.location.hash;
		var href = $('ul.wpm-language-switcher li a').attr('href');

		$('ul.wpm-language-switcher li a').each(function() {
			this.href = this.href+hash;
		});
	}

	/* forward to first project */
	/*if (window.location.href == "http://cosmic.es/" || 
		window.location.href == "http://www.cosmic.es/" ||
		window.location.href == "http://cosmic.es/es/" || 
		window.location.href == "http://www.cosmic.es/es/") {

		var gallery = "galeria/proyectos/";
		//var startHash = "#albert-y-ferran-adria";
		var startHash = setting.firstProject;
		console.log("first project", startHash);
		location = window.location.href + gallery + startHash;
	}*/

	/* remove hover for iOS */
	if ('ontouchstart' in document.documentElement) {
	    $('#main-wrap').removeClass('no-touch');
	}
});