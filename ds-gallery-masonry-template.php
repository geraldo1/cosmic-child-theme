<?php
/**
 *
 * Template Name: Masonry Gallery Page
 *
 * The template for displaying gallery page with masonry layout.
 *
 * @package dsframework
 * @since dsframework 1.0
 *
 */
?>
<?php get_header(); ?>
<div id="primary" class="site-content">
	<div id="content" role="main">
		<?php
		// NEW LINES
		//http://support.dimsemenov.com/forums/159023-touchfolio/suggestions/3253492-create-several-masonry-galleries?page=2&per_page=20
		global $wp_query;
		global $categories_to_show;
		$postid = $wp_query->post->ID;
		$cnt = get_post($postid);
		$cnt = get_object_vars($cnt);
		$categories_to_show = $cnt['post_content'];
		wp_reset_query();
		// END NEW

		// CHANGED LINE
		$gallery_cats = $categories_to_show;
		//$gallery_cats = get_ds_option('album_cats_gallery_page');
		// END CHANGED LINE

		if($gallery_cats) {
			$tax_query = array(
		    	'relation' => 'AND',
				array(
					'taxonomy' => 'ds-gallery-category',
					'field' => 'slug',
					'terms' => preg_split("/\s*,\s*/", $gallery_cats),
					'include_children' => true,
					'operator' => 'IN'
				)
		    );
		} else {
			$tax_query = '';
		}
		$loop = new WP_Query( array(
			'post_type' => 'ds-gallery',
			'posts_per_page' => -1,
			'tax_query' => $tax_query
		));
		?>
		<section class="albums-thumbnails clearfix">
		<div class="project-thumb-sizer"></div>
		<?php while ( $loop->have_posts() ) : $loop->the_post();  ?>
			<?php set_query_var( 'gallery_cats', $gallery_cats ); ?>
			<?php get_template_part( 'content', 'masonry' ); ?>
		<?php endwhile; ?>
		</section>
		<?php wp_reset_postdata(); ?>
		<?php //get_template_part( 'content', 'page-masonry' ); ?>
	</div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>